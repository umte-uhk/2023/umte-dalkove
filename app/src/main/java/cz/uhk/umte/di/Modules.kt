package cz.uhk.umte.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.uhk.umte.data.remote.ApiConfig
import cz.uhk.umte.data.remote.SpaceXApiService
import cz.uhk.umte.data.repositories.SpaceXRepository
import cz.uhk.umte.ui.launches.LaunchesViewModel
import cz.uhk.umte.ui.rocket.RocketDetailViewModel
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit

val appModules by lazy { listOf(dataModule, uiModule) }

val dataModule = module {
    apiServices()
    repositories()
}

val uiModule = module {
    viewModels()
}

private fun Module.viewModels() {
    viewModel { LaunchesViewModel(get()) }
    viewModel { (rocketId: String) -> RocketDetailViewModel(rocketId, get()) }
}

private fun Module.repositories() {
    single { SpaceXRepository(get()) }
}

private fun Module.apiServices() {
    single {
        createRetrofit(
            okHttpClient = OkHttpClient.Builder().build(),
            baseUrl = ApiConfig.BaseApiUrl,
        )
    }
    single {
        get<Retrofit>().create(SpaceXApiService::class.java)
    }
}

private val json = Json {
    ignoreUnknownKeys = true
}

private fun createRetrofit(
    okHttpClient: OkHttpClient,
    baseUrl: String,
): Retrofit {
    return Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .addConverterFactory(
            json.asConverterFactory(
                MediaType.get("application/json"),
            ),
        )
        .build()
}
