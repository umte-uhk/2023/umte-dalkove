package cz.uhk.umte.ui.home

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import cz.uhk.umte.ui.navigateToLaunches
import cz.uhk.umte.ui.navigateToLazyList

@Preview
@Composable // comp Enter
fun HomeScreen(
    parentController: NavHostController,
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Button(
            onClick = {
                parentController.navigateToLazyList()
            },
        ) {
            Text(text = "Lazy list")
        }
        Button(
            onClick = {
                parentController.navigateToLaunches()
            },
        ) {
            Text(text = "Launches")
        }
    }
}
