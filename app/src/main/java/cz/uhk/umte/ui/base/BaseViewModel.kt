package cz.uhk.umte.ui.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

sealed interface State {

    object None : State
    object Loading : State
    class Success(val any: Any? = null) : State
    class Failure(val throwable: Throwable) : State
}

abstract class BaseViewModel : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + Dispatchers.Default)

    private val _state = MutableStateFlow<State>(State.None)
    val state = _state.asStateFlow()

    protected fun <Result> launch(
        state: MutableStateFlow<State>? = _state,
        block: (suspend CoroutineScope.() -> Result),
    ) = scope.launch(
        CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
            state?.tryEmit(
                State.Failure(throwable),
            )
        },
    ) {
        // launch in progress
        state?.emit(State.Loading)

        // call our methods
        val result = block()

        // propagate results
        state?.emit(State.Success(result))
    }
}
