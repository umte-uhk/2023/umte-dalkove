package cz.uhk.umte.ui.rocket

import androidx.compose.foundation.layout.Column
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf

@Composable
fun RocketDetailScreen(
    rocketId: String?,
    viewModel: RocketDetailViewModel = getViewModel {
        parametersOf(rocketId ?: "")
    },
) {
    val detail = viewModel.detail.collectAsState()

    if (detail.value == null) {
        CircularProgressIndicator()
    } else {
        Column {
            Text(text = "Název rakety")
            Text(text = detail.value!!.name)
        }
    }
}
