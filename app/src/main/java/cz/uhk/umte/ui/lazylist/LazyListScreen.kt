package cz.uhk.umte.ui.lazylist

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Preview
@Composable
fun LazyListScreen() {
    val people = remember {
        generateRandomPeople(count = 400)
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
        contentPadding = PaddingValues(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        items(people) { human ->
            Row {
                Text(
                    text = human.name,
                    modifier = Modifier.weight(1F),
                )
                Icon(
                    imageVector = human.status.icon(),
                    contentDescription = null,
                )
            }
        }
    }
}

private fun generateRandomPeople(count: Int = 100) = mutableListOf<Human>()
    .apply {
        repeat(count) {
            add(
                Human(
                    name = "${names.random()} ${surnames.random()}",
                    status = Status.values().random(),
                ),
            )
        }
    }

private val names = arrayOf("Petr", "Tomáš", "Karel", "Lukáš")
private val surnames = arrayOf("Weissar", "Kozel", "Jones", "Smith")

data class Human(
    val name: String,
    val status: Status,
)

enum class Status {

    Okay, Favorite, NotOk;

    fun icon() = when (this) {
        Okay -> Icons.Default.ThumbUp
        Favorite -> Icons.Default.Favorite
        NotOk -> Icons.Default.Warning
    }
}
