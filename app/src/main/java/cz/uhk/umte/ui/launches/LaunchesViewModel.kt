package cz.uhk.umte.ui.launches

import cz.uhk.umte.data.model.RocketLaunchResponse
import cz.uhk.umte.data.repositories.SpaceXRepository
import cz.uhk.umte.ui.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class LaunchesViewModel(
    private val repo: SpaceXRepository,
) : BaseViewModel() {

    private val _launches = MutableStateFlow<List<RocketLaunchResponse>>(emptyList())
    val launches = _launches.asStateFlow()

    init {
        fetchLaunches()
    }

    private fun fetchLaunches() {
        launch {
            repo.fetchAllSuccessfulRocketLaunches().let {
                _launches.emit(it)
            }
        }
    }
}
