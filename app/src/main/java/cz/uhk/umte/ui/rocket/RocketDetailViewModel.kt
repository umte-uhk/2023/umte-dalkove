package cz.uhk.umte.ui.rocket

import cz.uhk.umte.data.model.RocketDetailResponse
import cz.uhk.umte.data.repositories.SpaceXRepository
import cz.uhk.umte.ui.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow

class RocketDetailViewModel(
    private val rocketId: String,
    private val repo: SpaceXRepository,
) : BaseViewModel() {

    val detail = MutableStateFlow<RocketDetailResponse?>(null)

    init {
        fetchRocketDetail()
    }

    private fun fetchRocketDetail() {
        launch {
            repo.fetchRocketDetail(rocketId).let {
                detail.emit(it)
            }
        }
    }
}
