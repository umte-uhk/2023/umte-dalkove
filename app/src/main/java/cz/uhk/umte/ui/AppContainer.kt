package cz.uhk.umte.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import cz.uhk.umte.ui.home.HomeScreen
import cz.uhk.umte.ui.launches.LaunchesScreen
import cz.uhk.umte.ui.lazylist.LazyListScreen
import cz.uhk.umte.ui.rocket.RocketDetailScreen

@Composable
fun AppContainer(
    controller: NavHostController,
) {
    NavHost(
        navController = controller,
        startDestination = DestinationHome,
    ) {
        composable(route = DestinationHome) {
            HomeScreen(parentController = controller)
        }
        composable(route = DestinationLazyList) {
            LazyListScreen()
        }
        composable(route = DestinationLaunches) {
            LaunchesScreen(parentController = controller)
        }
        composable(
            route = DestinationRocket,
            arguments = listOf(navArgument("rocketId") { type = NavType.StringType }),
        ) { entry ->
            val rocketId = entry.arguments?.getString("rocketId")
            RocketDetailScreen(rocketId = rocketId)
        }
    }
}

private const val DestinationHome = "home"
private const val DestinationLazyList = "lazy-list"
private const val DestinationLaunches = "launches"
private const val DestinationRocket = "rocket/{rocketId}"

fun NavHostController.navigateToLazyList() {
    navigate(DestinationLazyList)
}

fun NavHostController.navigateToLaunches() {
    navigate(DestinationLaunches)
}

fun NavHostController.navigateToRocket(rocketId: String) {
    navigate(DestinationRocket.replace("{rocketId}", rocketId))
}
