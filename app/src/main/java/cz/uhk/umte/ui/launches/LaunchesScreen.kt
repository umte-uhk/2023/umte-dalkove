package cz.uhk.umte.ui.launches

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import cz.uhk.umte.ui.base.State
import cz.uhk.umte.ui.navigateToRocket
import org.koin.androidx.compose.getViewModel

@Composable
fun LaunchesScreen(
    parentController: NavHostController,
    viewModel: LaunchesViewModel = getViewModel(),
) {
    val launches = viewModel.launches.collectAsState()
    val state = viewModel.state.collectAsState()

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        when (state.value) {
            State.None, State.Loading -> {
                CircularProgressIndicator()
            }

            is State.Failure -> {
                Text(text = "Nastala chyba")
            }

            is State.Success -> {
                LazyColumn {
                    items(launches.value) { launch ->
                        Row(
                            modifier = Modifier
                                .clickable {
                                    parentController.navigateToRocket(launch.rocket.rocketId)
                                }
                                .padding(16.dp),
                        ) {
                            Text(text = launch.missionName ?: "-")
                        }
                    }
                }
            }
        }
    }
}
