package cz.uhk.umte.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RocketDetailResponse(
    val id: Long,
    @SerialName("rocket_name")
    val name: String,
    @SerialName("active")
    val isActive: Boolean,
    val wikipedia: String?,
)