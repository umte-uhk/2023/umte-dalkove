package cz.uhk.umte.data.repositories

import cz.uhk.umte.data.remote.SpaceXApiService

class SpaceXRepository(
    private val api: SpaceXApiService,
) {

    suspend fun fetchAllSuccessfulRocketLaunches() =
        api.fetchAllLaunches(true)

    suspend fun fetchRocketDetail(rocketId: String) =
        api.fetchRocketDetail(rocketId)
}
