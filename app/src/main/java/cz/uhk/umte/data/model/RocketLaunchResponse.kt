package cz.uhk.umte.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RocketLaunchResponse(
    @SerialName("flight_number")
    val flightNumber: Int,
    @SerialName("mission_name")
    val missionName: String?,
    val rocket: Rocket,
) {

    @Serializable
    data class Rocket(
        @SerialName("rocket_id")
        val rocketId: String,
        @SerialName("rocket_name")
        val rocketName: String,
    )
}
