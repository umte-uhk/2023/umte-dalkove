package cz.uhk.umte.data.remote

import cz.uhk.umte.data.model.RocketDetailResponse
import cz.uhk.umte.data.model.RocketLaunchResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SpaceXApiService {

    @GET("launches")
    suspend fun fetchAllLaunches(
        @Query("launches_success") wasLaunchSuccessful: Boolean,
    ): List<RocketLaunchResponse>

    @GET("rockets/{rocketId}")
    suspend fun fetchRocketDetail(
        @Path("rocketId") rocketId: String,
    ): RocketDetailResponse?
}
